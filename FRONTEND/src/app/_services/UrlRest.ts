import { Injectable } from '@angular/core';

@Injectable()
export class URLRest {
    private url: string;

    constructor() {
        this.url = 'http://10.0.0.34:8085/';
    }

    getUrl(): string {
        return this.url;
    }
}