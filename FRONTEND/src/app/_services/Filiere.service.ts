import { Injectable } from '@angular/core';
import { Filiere } from './../_models/Filiere';
import { Http, Response, Headers } from '@angular/http';
import { URLRest } from './UrlRest';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class FiliereService {


    private _FiliereUrl = this.urlRest.getUrl() + 'filiere/getFiliers';
    private _FiliereByIdUrl = this.urlRest.getUrl() + 'filiere/getFilierById';

    constructor(private _http: Http, private urlRest: URLRest) {
    }

    getFilieres(): Observable<Filiere[]> {
        return this._http.get(this._FiliereUrl)
            .map((response: Response) => <Filiere[]>response.json())
            .catch(this.handleError);
    }

    getFiliereById(idFiliere): Observable<Filiere> {
        return this._http.get(this._FiliereByIdUrl+"/"+idFiliere)
            .map((response: Response) => <Filiere>response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error('ERROR : ', error);
        return Observable.throw(error.json().error || 'Server error');
    }
}