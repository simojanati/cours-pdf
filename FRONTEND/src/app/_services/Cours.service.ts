import { Injectable } from '@angular/core';
import { Cours } from './../_models/Cours';
import { UserDto } from './../_models/UserDto';
import { Http, Response, Headers } from '@angular/http';
import { URLRest } from './UrlRest';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class CoursService {


    private _CoursUrl = this.urlRest.getUrl() + 'cours/getCours';

    private _CoursFilterUrl = this.urlRest.getUrl() + 'cours/getCoursFilter';

    private _sendMailUrl = this.urlRest.getUrl() + 'mail/sendMail';

    constructor(private _http: Http, private urlRest: URLRest) {
    }

    getCoursByMatiere(idMatiere: number): Observable<Cours[]> {
        return this._http.get(this._CoursUrl + "/" + idMatiere)
            .map((response: Response) => <Cours[]>response.json())
            .catch(this.handleError);
    }

    getCoursByMatiereFilter(idMatiere: number, libelleMatiere:string): Observable<Cours[]> {
        return this._http.get(this._CoursFilterUrl + "/" + idMatiere + "/"+libelleMatiere)
            .map((response: Response) => <Cours[]>response.json())
            .catch(this.handleError);
    }

    sendMail(userDto:UserDto) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var result = this._http.post(this._sendMailUrl, userDto, { headers: headers })
            .map(res => res.json());
        return result;
    }

    private handleError(error: Response) {
        console.error('ERROR : ', error);
        return Observable.throw(error.json().error || 'Server error');
    }
}