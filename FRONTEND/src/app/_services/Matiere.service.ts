import { Injectable } from '@angular/core';
import { Matiere } from './../_models/Matiere';
import { Http, Response, Headers } from '@angular/http';
import { URLRest } from './UrlRest';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class MatiereService {


    private _MatiereUrl = this.urlRest.getUrl() + 'matiere/getMatiere';
    private _MatiereByIdUrl = this.urlRest.getUrl() + 'matiere/getMatiereById';

    constructor(private _http: Http, private urlRest: URLRest) {
    }

    getMatiereByFiliere(idFiliere: number): Observable<Matiere[]> {
        return this._http.get(this._MatiereUrl + "/" + idFiliere)
            .map((response: Response) => <Matiere[]>response.json())
            .catch(this.handleError);
    }

    getMatiereById(idMatiere: number): Observable<Matiere> {
        return this._http.get(this._MatiereByIdUrl + "/" + idMatiere)
            .map((response: Response) => <Matiere>response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error('ERROR : ', error);
        return Observable.throw(error.json().error || 'Server error');
    }
}