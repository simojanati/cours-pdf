import { Injectable } from '@angular/core';
import { User } from './../_models/User';
import { Http, Response, Headers } from '@angular/http';
import { URLRest } from './UrlRest';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserService {


    private _UserUrl = this.urlRest.getUrl() + 'user/authentification';

    constructor(private _http: Http, private urlRest: URLRest) {
    }

    authentification(login: string, password: string): Observable<User> {
        var pwdCrypte = btoa(password);
        return this._http.get(this._UserUrl + "?username=" + login + "&token=" + pwdCrypte)
            .map((response: Response) => <User>response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error('ERROR : ', error);
        return Observable.throw(error.json().error || 'Server error');
    }
}