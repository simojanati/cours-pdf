import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FiliereService } from '../_services/Filiere.service';
import { MatiereService } from '../_services/Matiere.service';
import { Filiere } from '../_models/Filiere';
import { Matiere } from '../_models/Matiere';

@Component({
  moduleId: module.id,
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {

  idFilierer: number;
  filiere: Filiere;
  matieres: Array<Matiere>;

  constructor(private route: ActivatedRoute, private _filiereService: FiliereService, private _matiereService: MatiereService) {
    this.filiere = {
      idFiliere: 1,
      libelleFiliere: "test",
      iconFiliere: "icone",
      imgFiliere:"img"
    };
   }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.idFilierer = Number.parseInt(params['filiere']);
      this._filiereService.getFiliereById(this.idFilierer).subscribe((data) => {
        this.filiere = data;
      });
      this._matiereService.getMatiereByFiliere(this.idFilierer).subscribe((data) => {
        this.matieres = data;
      });
    });
  }

}
