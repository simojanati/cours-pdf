import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClassesComponent } from './classes/classes.component';
import { CoursComponent } from './cours/cours.component';
import { MajFiliereComponent } from './maj-filiere/maj-filiere.component';
import { MajMatiereComponent } from './maj-matiere/maj-matiere.component';
import { MajCoursComponent } from './maj-cours/maj-cours.component';
import { MajUserComponent } from './maj-user/maj-user.component';
import { AuthGuard } from './_guards/index';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: '', component: HomeComponent, canActivate: [AuthGuard],
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'majFiliere', component: MajFiliereComponent, },
            { path: 'majMatiere', component: MajMatiereComponent },
            { path: 'majCours', component: MajCoursComponent },
            { path: 'majUser', component: MajUserComponent },
            { path: 'classes/:filiere', component: ClassesComponent },
            { path: 'cours/:matiere', component: CoursComponent },
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
        ]
    },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes,{ useHash: true });