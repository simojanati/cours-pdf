import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, BaseRequestOptions } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { routing } from './app.routing';
import { AuthGuard } from './_guards/index';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClassesComponent } from './classes/classes.component';
import { CoursComponent } from './cours/cours.component';

import { CoursService } from './_services/Cours.service';
import { FiliereService } from './_services/Filiere.service';
import { MatiereService } from './_services/Matiere.service';
import { UserService } from './_services/User.service';
import { URLRest } from './_services/UrlRest';
import { MajFiliereComponent } from './maj-filiere/maj-filiere.component';
import { MajMatiereComponent } from './maj-matiere/maj-matiere.component';
import { MajCoursComponent } from './maj-cours/maj-cours.component';
import { MajUserComponent } from './maj-user/maj-user.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DashboardComponent,
    ClassesComponent,
    CoursComponent,
    MajFiliereComponent,
    MajMatiereComponent,
    MajCoursComponent,
    MajUserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [
    AuthGuard,
    CoursService,
    FiliereService,
    MatiereService,
    UserService,
    URLRest,
    BaseRequestOptions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
