import { Component, OnInit } from '@angular/core';
import { User } from '../_models/User';

@Component({
  moduleId: module.id,
  templateUrl: './maj-user.component.html',
  styleUrls: ['./maj-user.component.css']
})
export class MajUserComponent implements OnInit {

  user: User;

  constructor() { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }

}
