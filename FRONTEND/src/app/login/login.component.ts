import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../_services/User.service';
import { User } from '../_models/User';

@Component({
  moduleId: module.id,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;
  public loading: boolean = false;
  public msgError: string = "";
  user: User;

  constructor(private router: Router, private _userService: UserService) { }

  ngOnInit() {
  }

  login() {
    if (this.username == undefined || this.username == "") {
      this.msgError = 'Nom d\'utilisateur est obligatoire';
      setTimeout(() => { this.msgError = ''; }, 3000);
    } else if (this.password == undefined || this.password == "") {
      this.msgError = 'Mot de passe est obligatoire';
      setTimeout(() => { this.msgError = ''; }, 3000);
    } else {
      this.loading = true;
      this._userService.authentification(this.username, this.password).subscribe((data) => {
        this.user = data;
        if (this.user.login != "vide" && this.user.email != "") {
          sessionStorage.setItem('currentUser', JSON.stringify(this.user));
          this.router.navigate(['/dashboard']);
        } else {
          this.msgError = 'Nom d\'utilisateur ou le mot de passe est incorrect';
          setTimeout(() => { this.msgError = ''; }, 3000);
        }
        this.loading = false;
      });
      setTimeout(() => {

      }, 3000);
    }
  }

  loginEvn(event: any) {
    if (event.keyCode == 13) {
      if (this.username == undefined || this.username == "") {
        this.msgError = 'Nom d\'utilisateur est obligatoire';
        setTimeout(() => { this.msgError = ''; }, 3000);
      } else if (this.password == undefined || this.password == "") {
        this.msgError = 'Mot de passe est obligatoire';
        setTimeout(() => { this.msgError = ''; }, 3000);
      } else {
        this.loading = true;
        this._userService.authentification(this.username, this.password).subscribe((data) => {
          this.user = data;
          if (this.user != null) {
            sessionStorage.setItem('currentUser', JSON.stringify(this.user));
            this.router.navigate(['/dashboard']);
          } else {
            this.msgError = 'Nom d\'utilisateur ou le mot de passe est incorrect';
            setTimeout(() => { this.msgError = ''; }, 3000);
          }
          this.loading = false;
        });
        setTimeout(() => {

        }, 3000);
      }
    }
  }


}
