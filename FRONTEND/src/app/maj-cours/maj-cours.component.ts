import { Component, OnInit } from '@angular/core';
import { User } from '../_models/User';

@Component({
  moduleId: module.id,
  templateUrl: './maj-cours.component.html',
  styleUrls: ['./maj-cours.component.css']
})
export class MajCoursComponent implements OnInit {
 
  user: User;

  constructor() { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }

}
