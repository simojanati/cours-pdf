import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajCoursComponent } from './maj-cours.component';

describe('MajCoursComponent', () => {
  let component: MajCoursComponent;
  let fixture: ComponentFixture<MajCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
