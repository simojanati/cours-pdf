import { Filiere } from './Filiere';

export interface Matiere{
    idMatiere:number;
    libelleMatiere:string;
    filiere:Filiere;
}