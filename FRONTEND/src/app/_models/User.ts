export interface User{
    idUser:number;
    nomPrenom:string;
    email:string;
    login:string;
    password:string;
    role:string;
}