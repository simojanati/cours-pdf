export interface Filiere{
    idFiliere:number;
    libelleFiliere:string;
    iconFiliere:string;
    imgFiliere:string;
}