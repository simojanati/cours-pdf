import { Matiere } from './Matiere';

export interface Cours{
    idCours:number;
    codeCours:string;
    libelleCours:string;
    imgCouverture:string;
    pdfSommaire:string;
    matiere:Matiere;
}