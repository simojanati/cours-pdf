import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajFiliereComponent } from './maj-filiere.component';

describe('MajFiliereComponent', () => {
  let component: MajFiliereComponent;
  let fixture: ComponentFixture<MajFiliereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajFiliereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajFiliereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
