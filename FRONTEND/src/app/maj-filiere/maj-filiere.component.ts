import { Component, OnInit, ElementRef } from '@angular/core';
import { FiliereService } from '../_services/Filiere.service';
import { Filiere } from '../_models/Filiere';
import { User } from '../_models/User';

@Component({
  moduleId: module.id,
  templateUrl: './maj-filiere.component.html',
  styleUrls: ['./maj-filiere.component.css']
})
export class MajFiliereComponent implements OnInit {

  filieres: Array<Filiere>;
  filiereUpdate: Filiere;
  user: User;
  msgError:string = "";
  msgSuccess:string = "";
  isLoad:boolean = false;

  constructor(private elementRef: ElementRef, private _filiereService: FiliereService) { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.refreshFiliers();
    this.filiereUpdate = {
      idFiliere: 0,
      libelleFiliere: "",
      iconFiliere: "",
      imgFiliere: ""
    };
  }

  refreshFiliers() {
    this._filiereService.getFilieres().subscribe((data) => {
      this.filieres = data;
    });
  }

  updateLigne(idFiliere: number, libelleFiliere: string, iconFiliere: string) {
    this.filiereUpdate = {
      idFiliere: idFiliere,
      libelleFiliere: libelleFiliere,
      iconFiliere: iconFiliere,
      imgFiliere: ""
    };
    console.log(this.filiereUpdate);
  }

}
