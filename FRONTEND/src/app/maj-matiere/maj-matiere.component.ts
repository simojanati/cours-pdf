import { Component, OnInit } from '@angular/core';
import { User } from '../_models/User';

@Component({
  moduleId: module.id,
  templateUrl: './maj-matiere.component.html',
  styleUrls: ['./maj-matiere.component.css']
})
export class MajMatiereComponent implements OnInit {

  user: User;

  constructor() { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }

}
