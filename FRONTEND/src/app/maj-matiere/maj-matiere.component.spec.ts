import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajMatiereComponent } from './maj-matiere.component';

describe('MajMatiereComponent', () => {
  let component: MajMatiereComponent;
  let fixture: ComponentFixture<MajMatiereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajMatiereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajMatiereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
