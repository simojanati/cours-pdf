import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoursService } from '../_services/Cours.service';
import { MatiereService } from '../_services/Matiere.service';
import { Cours } from '../_models/Cours';
import { Matiere } from '../_models/Matiere';
import { UserDto } from '../_models/UserDto';

@Component({
  moduleId: module.id,
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.css']
})
export class CoursComponent implements OnInit {

  idMatiere: number;
  matiere: Matiere;
  cours: Array<Cours>;
  heightWindow: number;
  widthWindow: number;

  nom: string;
  prenom: string;
  email: string;
  classe: string;
  userDto: UserDto;

  msgError: string = "";
  msgSuccess: string = "";
  isLoader: boolean = false;
  isVide: boolean = true;
  inputSearch: string;

  constructor(private route: ActivatedRoute, private _coursService: CoursService, private _matiereService: MatiereService) {
    this.matiere = {
      idMatiere: 1,
      libelleMatiere: "test",
      filiere: null
    };
  }

  search() {
    if (this.inputSearch != "") {
      this._coursService.getCoursByMatiereFilter(this.idMatiere, this.inputSearch).subscribe((data) => {
        this.cours = data;
        if (this.cours.length > 0) {
          this.isVide = false;
        } else {
          this.isVide = true;
        }
      });
    } else {
      this._coursService.getCoursByMatiere(this.idMatiere).subscribe((data) => {
        this.cours = data;
        if (this.cours.length > 0) {
          this.isVide = false;
        } else {
          this.isVide = true;
        }
      });
    }
  }

  sendMail(cour: string, codeCours:string, idCours: number) {
    this.isLoader = true;
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (this.nom === undefined || this.nom === ""
      || this.prenom === undefined || this.prenom === ""
      || this.classe === undefined || this.classe === ""
      || this.email === undefined || this.email === "") {
      this.isLoader = false;
      this.msgError = "Tout les champs sont obligatoire";
      setTimeout(() => { this.msgError = ''; }, 3000);
    } else if (!EMAIL_REGEXP.test(this.email)) {
      this.isLoader = false;
      this.msgError = "Veuillez vérifier le format de l'adresse mail";
      setTimeout(() => { this.msgError = ''; }, 3000);
    } else {
      this.userDto = {
        nomPrenom: this.nom + " " + this.prenom,
        classe: this.classe,
        cours: cour,
        codeCours: codeCours,
        email: this.email
      }
      var res: any;
      this._coursService.sendMail(this.userDto).subscribe((data) => {
        res = data;
        this.isLoader = false;
        if (res.message === "Error") {
          this.msgError = "Votre demande n'a pas ete envoyé, peux etre un probleme de connexion !";
          setTimeout(() => { this.msgError = ''; }, 3000);
        } else {
          this.msgSuccess = "Votre demande est envoyé avec success";
          setTimeout(() => {
            document.getElementById("myModalMail" + idCours).style.display = "none";
            this.msgSuccess = '';
          }, 3000);
        }

      });

    }

  }

  showPdf(id: string, url: string) {
    setTimeout(() => {
      document.getElementById("body" + id).innerHTML = "<object width='" + (this.widthWindow / 2) + "' height='" + (this.heightWindow - 100) + "'  data='" + url + "' type='application/pdf'></object>"
    }, 500);
  }

  ngOnInit() {
    this.heightWindow = window.innerHeight;
    this.widthWindow = window.innerWidth;
    this.route.params.subscribe(params => {
      this.idMatiere = Number.parseInt(params['matiere']);

      this._matiereService.getMatiereById(this.idMatiere).subscribe((data) => {
        this.matiere = data;
      });
      this._coursService.getCoursByMatiere(this.idMatiere).subscribe((data) => {
        this.cours = data;
        if (this.cours.length > 0) {
          this.isVide = false;
        } else {
          this.isVide = true;
        }
      });

    });
  }

}
