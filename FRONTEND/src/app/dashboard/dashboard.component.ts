import { Component, OnInit } from '@angular/core';
import { FiliereService } from '../_services/Filiere.service';
import { Filiere } from '../_models/Filiere';

@Component({
  moduleId: module.id,
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  filieres: Array<Filiere>;

  constructor(private _filiereService: FiliereService) { }

  ngOnInit() {
    this._filiereService.getFilieres().subscribe((data) => {
      this.filieres = data;
    });
  }

  active(element) {
    document.getElementById('accueil').className = '';
    document.getElementById('majFiliere').className = '';
    document.getElementById('majMatiere').className = '';
    document.getElementById('majCours').className = '';
    document.getElementById('majUser').className = '';
    this.filieres.forEach(f => {
      document.getElementById(f.libelleFiliere).className = '';
    });
    document.getElementById(element).className = 'active';
  }
}
