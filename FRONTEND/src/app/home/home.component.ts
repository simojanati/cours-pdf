import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FiliereService } from '../_services/Filiere.service';
import { Filiere } from '../_models/Filiere';
import { User } from '../_models/User';

@Component({
  moduleId: module.id,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  filieres: Array<Filiere>;
  user: User;

  constructor(private router: Router, private _filiereService: FiliereService) { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    if (sessionStorage.getItem("isRefresh") === null) {
      sessionStorage.setItem("isRefresh", "test");
      window.location.reload();
    } else {
      console.log("%cI%cS%cGA %cBibliothèque\n%cBy Nabil BARTOUT", "color: black; font-size:18px;font-weight: bold;", "color: red; font-size:18px;font-weight: bold;", "color: black; font-size:18px;font-weight: bold;", "color: black; font-size:16px;font-weight: bold;", "color: black; font-size:15px;font-weight: bold;");
    }
    this._filiereService.getFilieres().subscribe((data) => {
      this.filieres = data;
    });
  }

  logout() {
    sessionStorage.removeItem('currentUser');
    sessionStorage.removeItem("isRefresh")
    this.router.navigate(['/login']);
  }

  openPopup() {
    document.getElementById("mb-signout").style.display = "block";
  }

  closePopup() {
    document.getElementById("mb-signout").style.display = "none";
  }

  active(element) {
    document.getElementById('accueil').className = '';
    document.getElementById('majFiliere').className = '';
    document.getElementById('majMatiere').className = '';
    document.getElementById('majCours').className = '';
    document.getElementById('majUser').className = '';
    this.filieres.forEach(f => {
      document.getElementById(f.libelleFiliere).className = '';
    });
    document.getElementById(element).className = 'active';
  }

}
