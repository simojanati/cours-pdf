package com.isga;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.isga.model.Cours;
import com.isga.model.Filiere;
import com.isga.model.Matiere;
import com.isga.model.Responsable;
import com.isga.model.User;
import com.isga.service.ICoursService;
import com.isga.service.IFiliereService;
import com.isga.service.IMatiereService;
import com.isga.service.IResponsableService;
import com.isga.service.IUserService;

@SpringBootApplication
public class CoursPdfApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CoursPdfApplication.class, args);
	}

	@Autowired
	private IUserService userService;

	@Autowired
	private IResponsableService responsableService;

	@Autowired
	private ICoursService coursService;

	@Autowired
	private IFiliereService filiereService;

	@Autowired
	private IMatiereService matiereService;

	@Override
	public void run(String... arg0) throws Exception {

//		userService.addUser(new User("Nabil Bartout", "nabil@gmail.com", "nabil", "MTIzNDU2", "Admin"));
//		userService.addUser(new User("Etudiants isga", "isga@gmail.com", "isga", "MTExMTEx", "Etudiant"));
//
//		responsableService.addResponsable(new Responsable("simojanati92@gmail.com"));
//
//		Filiere filiere1 = filiereService
//				.addFiliere(new Filiere("Ingénierie", "fa-desktop", "../../assets/cours/Ingenierie_logo.png"));
//		Filiere filiere2 = filiereService
//				.addFiliere(new Filiere("Management", "fa-university", "../../assets/cours/management_logo.png"));
//
//		Matiere matiere1 = matiereService.addMatiere(new Matiere("Programmation", filiere1));
//		Matiere matiere2 = matiereService.addMatiere(new Matiere("Conception", filiere1));
//		Matiere matiere3 = matiereService.addMatiere(new Matiere("Electronique", filiere1));
//		Matiere matiere4 = matiereService.addMatiere(new Matiere("GESTION", filiere2));
//		Matiere matiere5 = matiereService.addMatiere(new Matiere("FINANCE", filiere2));
//
//		coursService.addCour(new Cours("100 questions pour comprendre et agir",
//				"../../assets/cours/GESTION/100 questions pour comprendre et agir/100 questions pour comprendre et agir.jpg",
//				"../../assets/cours/GESTION/100 questions pour comprendre et agir/Sommaire.pdf", matiere4));
//		coursService.addCour(new Cours("Controle de Gestion manuel et application",
//				"../../assets/cours/GESTION/Controle de Gestion manuel et application/controle de gestion manuel et application.jpg",
//				"../../assets/cours/GESTION/Controle de Gestion manuel et application/Sommaire.pdf", matiere4));
//		coursService.addCour(new Cours("Exercices gestion financière",
//				"../../assets/cours/GESTION/Exercices gestion financière/exercices gestion financière.jpg",
//				"../../assets/cours/GESTION/Exercices gestion financière/Sommaire.pdf", matiere4));
//		coursService.addCour(new Cours("Gérer la trésorerie et la relation bancaire",
//				"../../assets/cours/GESTION/Gérer la trésorerie et la relation bancaire/gérer la trésorerie et la relation bancaire.jpg",
//				"../../assets/cours/GESTION/Gérer la trésorerie et la relation bancaire/Table des matières.pdf",
//				matiere4));
//		coursService.addCour(new Cours("Gestion Financière de l'entreprise",
//				"../../assets/cours/GESTION/Gestion Financière de l'entreprise/Gestion financière de l'entreprise.jpg",
//				"../../assets/cours/GESTION/Gestion Financière de l'entreprise/Table des matières.pdf", matiere4));
//		coursService.addCour(new Cours("Gestion prévisionnelle et mesure de la performance",
//				"../../assets/cours/GESTION/Gestion prévisionnelle et mesure de la performance/Gestion prévisonnelle et mesure de la performance.jpg",
//				"../../assets/cours/GESTION/Gestion prévisionnelle et mesure de la performance/Gestion prévisionnelle et mesure de la performance.pdf",
//				matiere4));
//		coursService
//				.addCour(new Cours("Gestion Sociale", "../../assets/cours/GESTION/Gestion Sociale/Gestion Sociale .jpg",
//						"../../assets/cours/GESTION/Gestion Sociale/Table des matières.pdf", matiere4));
//		coursService.addCour(new Cours("la boite à outils de la maîtrise des risques en conception",
//				"../../assets/cours/GESTION/la boite à outils de la maîtrise des risques en conception/La boite à outils de la maîtrise des risques en conception.jpg",
//				"../../assets/cours/GESTION/la boite à outils de la maîtrise des risques en conception/Sommaire.pdf",
//				matiere4));
//		coursService.addCour(new Cours("La gestion du risque client dans les PME",
//				"../../assets/cours/GESTION/La gestion du risque client dans les PME/Untitled_Page_1.jpg",
//				"../../assets/cours/GESTION/La gestion du risque client dans les PME/Sommaire.pdf", matiere4));
//		coursService.addCour(new Cours("Le business plan du créateur d'entreprise",
//				"../../assets/cours/GESTION/Le business plan du créateur d'entreprise/Le business plan du créateur d'entreprise.jpg",
//				"../../assets/cours/GESTION/Le business plan du créateur d'entreprise/Sommaire.pdf", matiere4));
//		coursService.addCour(new Cours("Motiver les salariés du climat social à l'épargne salariale",
//				"../../assets/cours/GESTION/Motiver les salariés du climat social à l'épargne salariale/motiver les salariés.jpg",
//				"../../assets/cours/GESTION/Motiver les salariés du climat social à l'épargne salariale/Sommaire.pdf",
//				matiere4));
//		coursService.addCour(new Cours("Stratégie de l'entreprise",
//				"../../assets/cours/GESTION/Stratégie de l'entreprise/Stratégie de l'entreprise.jpg",
//				"../../assets/cours/GESTION/Stratégie de l'entreprise/Stratégie de l'entreprises.pdf", matiere4));
//		coursService.addCour(new Cours("Système d'information de gestion",
//				"../../assets/cours/GESTION/Système d'information de gestion/Système d'information de gestion.jpg",
//				"../../assets/cours/GESTION/Système d'information de gestion/Sommaire.pdf", matiere4));
//		coursService.addCour(
//				new Cours("Tous gestionnaires", "../../assets/cours/GESTION/Tous gestionnaires/tous gestionnaires.jpg",
//						"../../assets/cours/GESTION/Tous gestionnaires/Table des matières.pdf", matiere4));
//		coursService.addCour(new Cours("Vous avez dit supply chain",
//				"../../assets/cours/GESTION/Vous avez dit supply chain/vous avez dit supply chain.jpg",
//				"../../assets/cours/GESTION/Vous avez dit supply chain/Sommaire.pdf", matiere4));
//
//		coursService.addCour(
//				new Cours("Analyse Financière", "../../assets/cours/FINANCE/Analyse Financière/Analyse financière.jpg",
//						"../../assets/cours/FINANCE/Analyse Financière/Sommaire.pdf", matiere5));
//		coursService.addCour(new Cours("Analyse financière et gestion des valurs mobilières",
//				"../../assets/cours/FINANCE/Analyse financière et gestion des valurs mobilières/Analyse financière et gestion des valeurs mobilières.jpg",
//				"../../assets/cours/FINANCE/Analyse financière et gestion des valurs mobilières/Table des matières.pdf",
//				matiere5));
//		coursService.addCour(new Cours("Applications et cas de gestion financière",
//				"../../assets/cours/FINANCE/Applications et cas de gestion financière/Applications et cas de gestion financière.jpg",
//				"../../assets/cours/FINANCE/Applications et cas de gestion financière/Sommaire.pdf", matiere5));
//		coursService.addCour(new Cours("Bussiness plan en action",
//				"../../assets/cours/FINANCE/Bussiness plan en action/Business plan en action .jpg",
//				"../../assets/cours/FINANCE/Bussiness plan en action/SOMMAIRE.pdf", matiere5));
//		coursService.addCour(new Cours("Diagnostic Financier",
//				"../../assets/cours/FINANCE/Diagnostic Financier/Diagnostic Financier de l'entreprise.jpg",
//				"../../assets/cours/FINANCE/Diagnostic Financier/Sommaire.pdf", matiere5));
//		coursService.addCour(new Cours("Evaluation & Financement",
//				"../../assets/cours/FINANCE/Evaluation & Financement/Evaluation& Financement.jpg",
//				"../../assets/cours/FINANCE/Evaluation & Financement/Sommaire.pdf", matiere5));
//		coursService.addCour(
//				new Cours("Finance de Marche", "../../assets/cours/FINANCE/Finance de Marche/Finance de Marche.jpg",
//						"../../assets/cours/FINANCE/Finance de Marche/SOMMAIRE.pdf", matiere5));
//		coursService.addCour(new Cours("Finance d'entreprise",
//				"../../assets/cours/FINANCE/Finance d'entreprise/Finance d'entreprise.jpg",
//				"../../assets/cours/FINANCE/Finance d'entreprise/SOMMAIRE.pdf", matiere5));
//		coursService.addCour(new Cours("finance d'entreprise  1",
//				"../../assets/cours/FINANCE/finance d'entreprise  1/finance d'entreprise  1.jpg",
//				"../../assets/cours/FINANCE/finance d'entreprise  1/Sommaire.pdf", matiere5));
//		coursService.addCour(new Cours("Finance d'entreprise 2",
//				"../../assets/cours/FINANCE/Finance d'entreprise 2/Finance d'entreprise.jpg",
//				"../../assets/cours/FINANCE/Finance d'entreprise 2/Finance d'entreprise 2.pdf", matiere5));
//		coursService.addCour(new Cours("Gérer la trésorerie et la",
//				"../../assets/cours/FINANCE/Gérer la trésorerie et la/Gérer la trésorerie.jpg",
//				"../../assets/cours/FINANCE/Gérer la trésorerie et la/Table des Matières.pdf", matiere5));
//
//		coursService.addCour(new Cours("Gérer la trésorerie et la relation bancaire",
//				"../../assets/cours/FINANCE/Gérer la trésorerie et la relation bancaire/Gérer la trésorerie et la relation bancaire.jpg",
//				"../../assets/cours/FINANCE/Gérer la trésorerie et la relation bancaire/TABLE DES MATIERES.pdf",
//				matiere5));
//		coursService.addCour(new Cours("Gestion financiere",
//				"../../assets/cours/FINANCE/Gestion financiere/GESTION FINANCIERE.jpg",
//				"../../assets/cours/FINANCE/Gestion financiere/Table des matières  gestion financière.pdf", matiere5));
//		coursService.addCour(new Cours("Gestion Financière cas pratiques",
//				"../../assets/cours/FINANCE/Gestion Financière cas pratiques/Gestion Financière.jpg",
//				"../../assets/cours/FINANCE/Gestion Financière cas pratiques/Table de matières.pdf", matiere5));
//		coursService.addCour(new Cours("Gestion financière corriges du manuel",
//				"../../assets/cours/FINANCE/Gestion financière corriges du manuel/Gestion Financière.jpg",
//				"../../assets/cours/FINANCE/Gestion financière corriges du manuel/Table des matières.pdf", matiere5));
//		coursService.addCour(new Cours("L'audit pour tous réferences",
//				"../../assets/cours/FINANCE/L'audit pour tous réferences/L'audit pour tous.jpg",
//				"../../assets/cours/FINANCE/L'audit pour tous réferences/L'audit pour tous.pdf", matiere5));
//		coursService.addCour(new Cours(
//				"Le meilleur de l'analyse technique pour la gestion de vos portefeuilles boursiers",
//				"../../assets/cours/FINANCE/Le meilleur de l'analyse technique pour la gestion de vos portefeuilles boursiers/Le meilleur de l'analyse technique.jpg",
//				"../../assets/cours/FINANCE/Le meilleur de l'analyse technique pour la gestion de vos portefeuilles boursiers/Sommaire.pdf",
//				matiere5));
//		coursService.addCour(new Cours("l'essentiel à  connaître en gestion Financier",
//				"../../assets/cours/FINANCE/l'essentiel à  connaître en gestion Financier/Gestion Financier.jpg",
//				"../../assets/cours/FINANCE/l'essentiel à  connaître en gestion Financier/Sommaire.pdf", matiere5));
//		coursService.addCour(new Cours("L'evaluation des entreprises",
//				"../../assets/cours/FINANCE/L'evaluation des entreprises/l'évaluation des entreprises.jpg",
//				"../../assets/cours/FINANCE/L'evaluation des entreprises/Sommaire.pdf", matiere5));
//		coursService.addCour(new Cours("l'internationalisation des marchés de capitaux",
//				"../../assets/cours/FINANCE/l'internationalisation des marchés de capitaux/l'internationalisation des marchés de capitaux et de la détermination.jpg",
//				"../../assets/cours/FINANCE/l'internationalisation des marchés de capitaux/Sommaire.pdf", matiere5));
//		coursService.addCour(new Cours("Marchés Financiers Gestion de portefeuille et des risques",
//				"../../assets/cours/FINANCE/Marchés Financiers Gestion de portefeuille et des risques/Marchés Financiers gestion de portefeuille et des risques.jpg",
//				"../../assets/cours/FINANCE/Marchés Financiers Gestion de portefeuille et des risques/Table des matières.pdf",
//				matiere5));
//		coursService.addCour(new Cours("Monnaie banque et marchés financière",
//				"../../assets/cours/FINANCE/Monnaie banque et marchés financière/Monnaie banque et marchés financière.jpg",
//				"../../assets/cours/FINANCE/Monnaie banque et marchés financière/Table des matières.pdf", matiere5));
//		coursService.addCour(new Cours("Precis d'analyse de l'entreprise",
//				"../../assets/cours/FINANCE/Precis d'analyse de l'entreprise/Precis d'analyse financière de l'entreprise.jpg",
//				"../../assets/cours/FINANCE/Precis d'analyse de l'entreprise/Table des matieres.pdf", matiere5));
//		coursService.addCour(new Cours("SYSTEME BUDGETAIRE",
//				"../../assets/cours/FINANCE/SYSTEME BUDGETAIRE/SYSTEME BUDGETAIRE POUR LE CONTROLE DE GESTION_Page_13.jpg",
//				"../../assets/cours/FINANCE/SYSTEME BUDGETAIRE/table de matiere  systeme budgetaire pour le controle de gestion.pdf",
//				matiere5));
//		coursService.addCour(new Cours("Tableaux de bord des managers",
//				"../../assets/cours/FINANCE/Tableaux de bord des managers/Tableaux de bord.jpg",
//				"../../assets/cours/FINANCE/Tableaux de bord des managers/Sommaire.pdf", matiere5));

	}
}
