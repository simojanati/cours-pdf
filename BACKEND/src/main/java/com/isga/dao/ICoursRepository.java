package com.isga.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.isga.model.Cours;

public interface ICoursRepository extends JpaRepository<Cours, Long> {

	
	@Query(nativeQuery = true, value = "SELECT * FROM cours c WHERE c.id_matiere=:x order by libelle_cours")
	public List<Cours> getCoursByMatiere(@Param("x") Long idMatiere);
	
	@Query(nativeQuery = true, value = "SELECT * FROM cours c WHERE c.id_matiere=:x and c.libelle_cours like :y order by libelle_cours")
	public List<Cours> getCoursByMatiereAndFilter(@Param("x") Long idMatiere, @Param("y") String libelleCours);
	
}
