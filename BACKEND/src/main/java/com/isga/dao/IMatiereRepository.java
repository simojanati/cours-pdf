package com.isga.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.isga.model.Matiere;

public interface IMatiereRepository extends JpaRepository<Matiere, Long> {

	@Query(nativeQuery = true, value = "SELECT * FROM matiere m WHERE m.id_filiere=:x order by m.libelle_matiere")
	public List<Matiere> getMatieresByFiliere(@Param("x") Long idFiliere);
	
	@Query(nativeQuery = true, value = "SELECT * FROM matiere m WHERE m.id_filiere=:x and m.libelle_matiere=:y order by m.libelle_matiere")
	public List<Matiere> getMatieresByFiliereFilter(@Param("x") Long idFiliere, @Param("y") String libelleMatiere);
	
}
