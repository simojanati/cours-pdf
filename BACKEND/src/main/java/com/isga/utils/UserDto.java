package com.isga.utils;

public class UserDto {

	private String nomPrenom;
	private String email;
	private String cours;
	private String codeCours;
	private String classe;

	public String getNomPrenom() {
		return nomPrenom;
	}

	public void setNomPrenom(String nomPrenom) {
		this.nomPrenom = nomPrenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCours() {
		return cours;
	}

	public void setCours(String cours) {
		this.cours = cours;
	}

	public String getCodeCours() {
		return codeCours;
	}

	public void setCodeCours(String codeCours) {
		this.codeCours = codeCours;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public UserDto(String nomPrenom, String email, String cours, String codeCours, String classe) {
		super();
		this.nomPrenom = nomPrenom;
		this.email = email;
		this.cours = cours;
		this.codeCours = codeCours;
		this.classe = classe;
	}

	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "UserDto [nomPrenom=" + nomPrenom + ", email=" + email + ", cours=" + cours + ", codeCours=" + codeCours
				+ ", classe=" + classe + "]";
	}

}
