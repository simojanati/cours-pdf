package com.isga.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.isga.model.Filiere;
import com.isga.service.IFiliereService;

@RestController
@CrossOrigin
@RequestMapping("/filiere")
public class FiliereRestController {

	@Autowired
	private IFiliereService filiereService;

	@GetMapping("/getFiliers")
	public List<Filiere> getFilieres() {
		try {
			return filiereService.getAllFilieres();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@PostMapping("/upload")
	public String singleFileUpload(@RequestParam("file") MultipartFile file) {

		if (file.isEmpty()) {
			return "{\"message\": \"Error : fichier vide\"}";
		}

		try {

			byte[] bytes = file.getBytes();
			Path path = Paths.get(getPathUpload() + file.getOriginalFilename());
			Files.write(path, bytes);

			return "{\"message\": \"OK\"}";

		} catch (IOException e) {
			System.out.println(e.getMessage());
			return "{\"message\": \"Error : " + e.getMessage() + "\"}";
		}

	}

	private String getPathUpload() {
		String test = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		String result = "";
		String[] tab = test.split("/");
		for (int i = 1; i < tab.length; i++) {
			if (!tab[i].equals("BACKEND")) {
				result += tab[i] + "/";
			} else {
				break;
			}
		}
		result += "FRONTEND/src/assets/cours/";
		return result;
	}

	@GetMapping("/getFilierById/{idFiliere}")
	public Filiere getFilierById(@PathVariable("idFiliere") Long idFiliere) {
		try {
			return filiereService.getFiliereById(idFiliere);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@GetMapping("/addFiliere")
	public Filiere addFiliere(@RequestAttribute("libelleFiliere") String libelleFiliere,
			@RequestAttribute("imgFiliere") String imgFiliere, @RequestAttribute("iconFiliere") String iconFiliere) {
		Filiere filiere = new Filiere(libelleFiliere, iconFiliere, imgFiliere);
		Filiere filiereRes = filiereService.addFiliere(filiere);
		return filiereRes;
	}

	@GetMapping("/updateFiliere")
	public Filiere updateFiliere(@RequestAttribute("idFiliere") Long idFiliere,
			@RequestAttribute("libelleFiliere") String libelleFiliere,
			@RequestAttribute("imgFiliere") String imgFiliere, @RequestAttribute("iconFiliere") String iconFiliere) {
		Filiere filiere = filiereService.getFiliereById(idFiliere);
		filiere.setImgFiliere(imgFiliere);
		filiere.setLibelleFiliere(libelleFiliere);
		filiere.setIconFiliere(iconFiliere);
		Filiere filiereRes = filiereService.updateFiliere(filiere);
		return filiereRes;
	}

	@GetMapping("/deleteFiliere/{idFiliere}")
	public String deleteFiliere(@PathVariable("idFiliere") Long idFiliere) {
		boolean res = filiereService.deleteFiliere(idFiliere);
		if (res) {
			return "{\"message\": \"OK\"}";
		} else {
			return "{\"message\": \"Error\"}";
		}
	}
}
