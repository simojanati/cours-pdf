package com.isga.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isga.model.Filiere;
import com.isga.model.Matiere;
import com.isga.service.IFiliereService;
import com.isga.service.IMatiereService;

@RestController
@CrossOrigin
@RequestMapping("/matiere")
public class MatiereRestController {

	@Autowired
	private IMatiereService matiereService;

	@Autowired
	private IFiliereService filiereService;

	@GetMapping("/getMatiere/{idFiliere}")
	public List<Matiere> getMatiereByFiliere(@PathVariable("idFiliere") Long idFiliere) {
		try {
			return matiereService.getAllMatieresByFiliere(idFiliere);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@GetMapping("/getMatiereById/{idMatiere}")
	public Matiere getMatiereById(@PathVariable("idMatiere") Long idMatiere) {
		try {
			return matiereService.getMatiereById(idMatiere);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@GetMapping("/addMatiere")
	public Matiere addMatiere(@RequestAttribute("libelleMatiere") String libelleMatiere,
			@RequestAttribute("idFiliere") Long idFiliere) {
		Filiere filiere = filiereService.getFiliereById(idFiliere);
		Matiere matiere = new Matiere(libelleMatiere, filiere);
		Matiere matiereRes = matiereService.updateMatiere(matiere);
		return matiereRes;
	}

	@GetMapping("/updateMatiere")
	public Matiere updateMatiere(@RequestAttribute("idMatiere") Long idMatiere,
			@RequestAttribute("libelleMatiere") String libelleMatiere, @RequestAttribute("idFiliere") Long idFiliere) {
		Filiere filiere = filiereService.getFiliereById(idFiliere);
		Matiere matiere = matiereService.getMatiereById(idMatiere);
		matiere.setLibelleMatiere(libelleMatiere);
		matiere.setFiliere(filiere);
		Matiere matiereRes = matiereService.updateMatiere(matiere);
		return matiereRes;
	}

	@GetMapping("/deleteMatiere/{idMatiere}")
	public String deleteMatiere(@PathVariable("idMatiere") Long idMatiere) {
		boolean res = matiereService.deleteMatiere(idMatiere);
		if (res) {
			return "{\"message\": \"OK\"}";
		} else {
			return "{\"message\": \"Error\"}";
		}
	}
}
