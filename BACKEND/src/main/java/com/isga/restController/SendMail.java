package com.isga.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isga.service.IResponsableService;
import com.isga.utils.UserDto;

@RestController
@CrossOrigin
@RequestMapping("/mail")
public class SendMail {

	@Autowired
	public MailSender mailSender;

	@Autowired
	public IResponsableService responsableService;

	@PostMapping(value = "/sendMail")
	public String triggerEmail(@RequestBody UserDto userDto) {
		String sendTo = responsableService.getEmail();
		SimpleMailMessage message = new SimpleMailMessage();
		String msg = "Nom / prénom : ";
		msg += userDto.getNomPrenom() + "\n";
		msg += userDto.getEmail() + "\n";
		msg += "Classe : ";
		msg += userDto.getClasse() + "\n";
		msg += "Livre : ";
		msg += userDto.getCours() + "\n";
		msg += "Code Livre : ";
		msg += userDto.getCodeCours();
		message.setSubject("Demande reservation d'un livre");
		message.setText(msg);
		message.setTo(sendTo);
		message.setFrom(userDto.getEmail());
		try {
			mailSender.send(message);
			return "{\"message\": \"OK\"}";
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"message\": \"Error\"}";
		}
	}
}
