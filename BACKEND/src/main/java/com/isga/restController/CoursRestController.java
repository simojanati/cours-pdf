package com.isga.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isga.model.Cours;
import com.isga.model.Matiere;
import com.isga.service.ICoursService;
import com.isga.service.IMatiereService;

@RestController
@CrossOrigin
@RequestMapping("/cours")
public class CoursRestController {

	@Autowired
	private ICoursService coursService;

	@Autowired
	private IMatiereService matiereService;

	@GetMapping("/getCours/{idMatiere}")
	public List<Cours> getCours(@PathVariable("idMatiere") Long idMatiere) {
		try {
			return coursService.getAllCoursByMatiere(idMatiere);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@GetMapping("/getCoursFilter/{idMatiere}/{libelleCours}")
	public List<Cours> getCoursFilter(@PathVariable("idMatiere") Long idMatiere,
			@PathVariable("libelleCours") String libelleCours) {
		try {
			return coursService.getAllCoursByMatiereAndFilter(idMatiere, libelleCours);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@GetMapping("/getAllCours")
	public List<Cours> getAllCours() {
		try {
			return coursService.getAllCours();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@GetMapping("/addCours")
	public Cours addCours(@RequestAttribute("libelleCours") String libelleCours,
			@RequestAttribute("imgCouverture") String imgCouverture,
			@RequestAttribute("codeCours") String codeCours,
			@RequestAttribute("pdfSommaire") String pdfSommaire, @RequestAttribute("idMatiere") Long idMatiere) {
		Matiere matiere = matiereService.getMatiereById(idMatiere);
		Cours cours = new Cours(libelleCours, codeCours, imgCouverture, pdfSommaire, matiere);
		Cours coursRes = coursService.addCour(cours);
		return coursRes;
	}

	@GetMapping("/updateCours")
	public Cours updateCours(@RequestAttribute("idCours") Long idCours,
			@RequestAttribute("libelleCours") String libelleCours,
			@RequestAttribute("imgCouverture") String imgCouverture,
			@RequestAttribute("pdfSommaire") String pdfSommaire, @RequestAttribute("idMatiere") Long idMatiere) {
		Matiere matiere = matiereService.getMatiereById(idMatiere);
		Cours cours = coursService.getCoursById(idCours);
		cours.setLibelleCours(libelleCours);
		cours.setImgCouverture(imgCouverture);
		cours.setPdfSommaire(pdfSommaire);
		cours.setMatiere(matiere);
		Cours coursRes = coursService.updateCours(cours);
		return coursRes;
	}

	@GetMapping("/deleteCours/{idCours}")
	public String deleteCours(@PathVariable("idCours") Long idCours) {
		boolean res = coursService.deleteCours(idCours);
		if (res) {
			return "{\"message\": \"OK\"}";
		} else {
			return "{\"message\": \"Error\"}";
		}
	}
}
