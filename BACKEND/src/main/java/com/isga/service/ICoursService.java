package com.isga.service;

import java.util.List;

import com.isga.model.Cours;

public interface ICoursService {

	public Cours addCour(Cours cours);

	public Cours updateCours(Cours cours);

	public boolean deleteCours(Long idCours);

	public Cours getCoursById(Long idCours);

	public List<Cours> getAllCours();

	public List<Cours> getAllCoursByMatiere(Long idMatiere);
	
	public List<Cours> getAllCoursByMatiereAndFilter(Long idMatiere, String libelleCours);

}
