package com.isga.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isga.dao.ICoursRepository;
import com.isga.model.Cours;
import com.isga.service.ICoursService;

@Service
@Transactional
public class CoursService implements ICoursService {

	@Autowired
	private ICoursRepository coursRepository;

	@Override
	public Cours addCour(Cours cours) {
		return coursRepository.save(cours);
	}

	@Override
	public List<Cours> getAllCoursByMatiere(Long idMatiere) {
		List<Cours> cours = coursRepository.getCoursByMatiere(idMatiere);
		return cours;
	}

	@Override
	public Cours updateCours(Cours cours) {
		return coursRepository.save(cours);
	}

	@Override
	public boolean deleteCours(Long idCours) {
		try {
			coursRepository.delete(idCours);
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	@Override
	public List<Cours> getAllCours() {
		return coursRepository.findAll();
	}

	@Override
	public Cours getCoursById(Long idCours) {
		return coursRepository.findOne(idCours);
	}

	@Override
	public List<Cours> getAllCoursByMatiereAndFilter(Long idMatiere, String libelleCours) {
		libelleCours = "%" + libelleCours + "%";
		return coursRepository.getCoursByMatiereAndFilter(idMatiere, libelleCours);
	}

}
